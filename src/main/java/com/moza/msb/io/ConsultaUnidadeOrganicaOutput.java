package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.UnidadesOrganicas;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class ConsultaUnidadeOrganicaOutput extends ServiceOutput {

	private UnidadesOrganicas unidadesOrganicas;

	public UnidadesOrganicas getUnidadesOrganicas() {
		return this.unidadesOrganicas;
	}

	public void setUnidadesOrganicas(UnidadesOrganicas unidadesOrganicas) {
		this.unidadesOrganicas = unidadesOrganicas;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaUnidadeOrganicaOutput [unidadesOrganicas=");
		builder.append(unidadesOrganicas);
		builder.append("]");
		return builder.toString();
	}

}
