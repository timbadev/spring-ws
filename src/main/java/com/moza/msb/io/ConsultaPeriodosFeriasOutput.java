package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.PeriodosFerias;

public class ConsultaPeriodosFeriasOutput extends ServiceOutput {

	private PeriodosFerias periodoFerias;

	public PeriodosFerias getPeriodoFerias() {
		return periodoFerias;
	}

	public void setPeriodoFerias(PeriodosFerias periodoFerias) {
		this.periodoFerias = periodoFerias;
	}

}
