package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.Ausencias;

public class ConsultaAusenciasOutput extends ServiceOutput {

	private Ausencias ausencias;

	public Ausencias getAusencias() {
		return ausencias;
	}

	public void setAusencias(Ausencias ferias) {
		this.ausencias = ferias;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaAusenciasOutput [ausencias=");
		builder.append(ausencias);
		builder.append("]");
		return builder.toString();
	}

}
