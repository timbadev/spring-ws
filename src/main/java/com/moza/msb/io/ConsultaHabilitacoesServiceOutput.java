package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.Habilitacoes;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class ConsultaHabilitacoesServiceOutput extends ServiceOutput {

	private Habilitacoes habilitacoes;

	public Habilitacoes getHabilitacoes() {
		return this.habilitacoes;
	}

	public void setHabilitacoes(Habilitacoes habilitacoes) {
		this.habilitacoes = habilitacoes;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaHabilitacoesServiceOutput [habilitacoes=");
		builder.append(habilitacoes);
		builder.append("]");
		return builder.toString();
	}

}
