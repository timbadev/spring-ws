package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.Posicao;

import java.util.List;

public class ConsultaPosicaoOutput extends ServiceOutput {
	private List<Posicao> consultaPosicao;

	public List<Posicao> getConsultaPosicao() {
		return this.consultaPosicao;
	}

	public void setConsultaPosicao(List<Posicao> consultaPosicao) {
		this.consultaPosicao = consultaPosicao;
	}
}
