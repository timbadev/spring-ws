package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceInput;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class ConsultaColaboradoresInput extends ServiceInput {

	private String numColaborador;
	private String categoria;

	public String getNumColaborador() {
		return numColaborador;
	}

	public void setNumColaborador(String numColaborador) {
		this.numColaborador = numColaborador;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String direccao) {
		this.categoria = direccao;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaColaboradoresInput [numColaborador=");
		builder.append(numColaborador);
		builder.append(", categoria=");
		builder.append(categoria);
		builder.append("]");
		return builder.toString();
	}
}
