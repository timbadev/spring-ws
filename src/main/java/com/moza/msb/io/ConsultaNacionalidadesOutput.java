package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.Nacionalidades;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class ConsultaNacionalidadesOutput extends ServiceOutput {

	private Nacionalidades nacionalidades;

	public Nacionalidades getNacionalidades() {
		return this.nacionalidades;
	}

	public void setNacionalidades(Nacionalidades nacionalidades) {
		this.nacionalidades = nacionalidades;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaNacionalidadesOutput [nacionalidades=");
		builder.append(nacionalidades);
		builder.append("]");
		return builder.toString();
	}


}
