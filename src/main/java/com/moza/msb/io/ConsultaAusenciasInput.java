package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceInput;

public class ConsultaAusenciasInput extends ServiceInput {

	private String dataInicio;
	private String numColaborador;
	private String ausencia;

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getNumColaborador() {
		return numColaborador;
	}

	public void setNumColaborador(String numColaborador) {
		this.numColaborador = numColaborador;
	}

	public String getAusencia() {
		return ausencia;
	}

	public void setAusencia(String ausencia) {
		this.ausencia = ausencia;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaAusenciasInput [dataInicio=");
		builder.append(dataInicio);
		builder.append(", numColaborador=");
		builder.append(numColaborador);
		builder.append(", ausencia=");
		builder.append(ausencia);
		builder.append("]");
		return builder.toString();
	}


}
