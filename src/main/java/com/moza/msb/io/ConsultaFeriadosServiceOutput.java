package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.FeriadosMunicipais;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class ConsultaFeriadosServiceOutput extends ServiceOutput {
	private FeriadosMunicipais feriados;

	public ConsultaFeriadosServiceOutput(FeriadosMunicipais feriados) {
		super();
		this.feriados = feriados;
	}

	public ConsultaFeriadosServiceOutput() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FeriadosMunicipais getFeriados() {
		return this.feriados;
	}

	public void setFeriados(FeriadosMunicipais feriados) {
		this.feriados = feriados;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaFeriadosServiceOutput [feriados=");
		builder.append(feriados);
		builder.append("]");
		return builder.toString();
	}


}
