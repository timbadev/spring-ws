package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceInput;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class ConsultaFeriadosServiceInput extends ServiceInput {

	private String municipio;
	private String ano;

	public String getAno() {
		return this.ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getMunicipio() {
		return this.municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaFeriadosServiceInput [municipio=");
		builder.append(municipio);
		builder.append(", ano=");
		builder.append(ano);
		builder.append("]");
		return builder.toString();
	}

}
