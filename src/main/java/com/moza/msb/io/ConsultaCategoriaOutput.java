package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.Categoria;

import java.util.List;

public class ConsultaCategoriaOutput extends ServiceOutput {
	private List<Categoria> categoria;

	public List<Categoria> getCategoria() {
		return this.categoria;
	}

	public void setCategoria(List<Categoria> categoria) {
		this.categoria = categoria;
	}
}
