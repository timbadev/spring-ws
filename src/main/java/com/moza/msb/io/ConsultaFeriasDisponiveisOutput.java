package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.FeriasDisponiveis;

public class ConsultaFeriasDisponiveisOutput extends ServiceOutput {

	private FeriasDisponiveis feriasDisponiveis;

	public FeriasDisponiveis getFeriasDisponiveis() {
		return feriasDisponiveis;
	}

	public void setFeriasDisponiveis(FeriasDisponiveis feriasDisponiveis) {
		this.feriasDisponiveis = feriasDisponiveis;
	}

}
