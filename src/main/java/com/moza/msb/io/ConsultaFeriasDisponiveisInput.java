package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceInput;

public class ConsultaFeriasDisponiveisInput extends ServiceInput {
	private String numColaborador;

	public String getNumColaborador() {
		return numColaborador;
	}

	public void setNumColaborador(String numColaborador) {
		this.numColaborador = numColaborador;
	}

}
