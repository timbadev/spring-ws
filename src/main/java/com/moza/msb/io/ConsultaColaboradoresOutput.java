package com.moza.msb.io;

import com.moza.msb.common.beans.ServiceOutput;
import com.moza.msb.bean.Colaboradores;

public class ConsultaColaboradoresOutput extends ServiceOutput {
	
	protected Colaboradores colaboradores;

    public Colaboradores getColaboradores() {
        return colaboradores;
    }

    public void setColaboradores(Colaboradores value) {
        this.colaboradores = value;
    }
}
