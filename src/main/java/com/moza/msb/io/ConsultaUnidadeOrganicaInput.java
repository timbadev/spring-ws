package com.moza.msb.io;


import com.moza.msb.common.beans.ServiceInput;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class ConsultaUnidadeOrganicaInput extends ServiceInput {
	private String balcao;

	public String getBalcao() {
		return this.balcao;
	}

	public void setBalcao(String balcao) {
		this.balcao = balcao;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ConsultaUnidadeOrganicaInput [balcao=");
		builder.append(balcao);
		builder.append("]");
		return builder.toString();
	}

}
