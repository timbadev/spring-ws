package com.moza.msb.service;

import com.moza.msb.common.StatusValidator;
import com.moza.msb.common.beans.ServiceInput;
import com.moza.msb.common.exceptions.ServiceException;
import com.moza.msb.bean.Colaboradores;
import com.moza.msb.io.*;
import com.moza.msb.provider.SapHrProvider;
import org.apache.log4j.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(serviceName = "ConsultaSap", name = "listarColaboradores", targetNamespace = "http://msb.mozabanco.co.mz")
public class ConsultaSapSS {

	private static Logger logger = Logger.getLogger(ConsultaSapSS.class);


	@WebMethod(operationName = "listarColaboradores",action = "listarColaboradores")
	@WebResult(name = "output", targetNamespace = "http://msb.mozabanco.co.mz")
	public ConsultaColaboradoresOutput listarColaboradores(
			@WebParam(name = "input", targetNamespace = "http://msb.mozabanco.co.mz") ConsultaColaboradoresInput input) {

		ConsultaColaboradoresOutput serviceOutput = new ConsultaColaboradoresOutput();
		try {
			logger.debug("consulta Colaborador input:" + input);

			// create provider
			final SapHrProvider hrProvider = new SapHrProvider();
			// retrieve data
			Colaboradores colabs = new Colaboradores();
			colabs.setColaborador(hrProvider.findColaborador(input));
			serviceOutput.setColaboradores(colabs);
			logger.debug("consultaColaborador output:" + serviceOutput);

		} catch (final ServiceException e) {
			e.printStackTrace();
			logger.error("consulta Colaborador error ", e);
			serviceOutput = new ConsultaColaboradoresOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessages()));
		} catch (final Exception e) {
			e.printStackTrace();
			logger.error("consulta Colaborador error ", e);
			serviceOutput = new ConsultaColaboradoresOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessage()));
		}

		return serviceOutput;
	}

	@WebMethod(operationName = "listarCategoria")
	@WebResult(name = "output", targetNamespace = "http://msb.mozabanco.co.mz")
	public ConsultaCategoriaOutput consultaCategoria(
			@WebParam(name = "input", targetNamespace = "http://msb.mozabanco.co.mz") ConsultaCategoriaInput input) {
		ConsultaCategoriaOutput serviceOutput = new ConsultaCategoriaOutput();
		try {
			logger.debug("consulta Categoria input:" + input);

			// create provider
			final SapHrProvider hrProvider = new SapHrProvider();
			// retrieve data
			serviceOutput.setCategoria(hrProvider.consultaCategoria(input));

			logger.debug("consulta Categoria output:" + serviceOutput);

		} catch (final ServiceException e) {
			e.printStackTrace();
			logger.error("consulta Categoria error ", e);
			serviceOutput = new ConsultaCategoriaOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessages()));
		} catch (final Exception e) {
			e.printStackTrace();
			logger.error("consulta Categoria error ", e);
			serviceOutput = new ConsultaCategoriaOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessage()));
		}
		return serviceOutput;
	}

	@WebMethod(operationName = "listarPosicao")
	@WebResult(name = "output", targetNamespace = "http://msb.mozabanco.co.mz")
	public ConsultaPosicaoOutput consultaPosicao(
			@WebParam(name = "input", targetNamespace = "http://msb.mozabanco.co.mz") ConsultaPosicaoInput input) {
		ConsultaPosicaoOutput serviceOutput = new ConsultaPosicaoOutput();
		try {
			logger.debug("consultaPosicao input:" + input);

			// create provider
			final SapHrProvider hrProvider = new SapHrProvider();
			// retrieve data
			serviceOutput.setConsultaPosicao(hrProvider.consultaPosicao(input));

			logger.debug("consultaPosicao output:" + serviceOutput);

		} catch (final ServiceException e) {
			e.printStackTrace();
			logger.error("consultaPosicao error ", e);
			serviceOutput = new ConsultaPosicaoOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessages()));
		} catch (final Exception e) {
			e.printStackTrace();
			logger.error("consultaPosicao error ", e);
			serviceOutput = new ConsultaPosicaoOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessage()));
		}
		return serviceOutput;
	}

	@WebMethod(operationName = "listarUnidadesOrganicas")
	@WebResult(name = "output", targetNamespace = "http://msb.mozabanco.co.mz")
	public ConsultaUnidadeOrganicaOutput consultaUnidadesOrganicas(
			@WebParam(name = "input", targetNamespace = "http://msb.mozabanco.co.mz") ConsultaUnidadeOrganicaInput input) {

		ConsultaUnidadeOrganicaInput serviceInput = null;
		ConsultaUnidadeOrganicaOutput serviceOutput = null;
		try {
			serviceInput = input;
			logger.debug("Consultar Unidades Organicas input:" + serviceInput);

			// create provider
			final SapHrProvider hrProvider = new SapHrProvider();

			// retrieve data
			serviceOutput = hrProvider.findUnidadesOrganicas(serviceInput);
			logger.debug("Consultar Unidades Organicas output:" + serviceOutput);

		} catch (final ServiceException e) {
			e.printStackTrace();
			logger.error("Consultar Unidades Organicas error ", e);
			serviceOutput = new ConsultaUnidadeOrganicaOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessages()));
		} catch (final Exception e) {
			e.printStackTrace();
			logger.error("Consultar Unidades Organicas error ", e);

			serviceOutput = new ConsultaUnidadeOrganicaOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessage()));
		}

		return serviceOutput;
	}

	@WebMethod(operationName = "listarHabilitacoes")
	@WebResult(name = "output", targetNamespace = "http://msb.mozabanco.co.mz")
	public ConsultaHabilitacoesServiceOutput consultaHabilitacoes(
			@WebParam(name = "input", targetNamespace = "http://msb.mozabanco.co.mz") ServiceInput input) {

		ConsultaHabilitacoesServiceOutput serviceOutput = null;

		try {
			logger.debug("Consultar Habilitacoes input:" + input);

			// create provider
			final SapHrProvider hrProvider = new SapHrProvider();
			// retrieve data
			serviceOutput = hrProvider.getHabilitacoes(input);

			logger.debug("Consultar Habilitacoes output:" + serviceOutput);

			// retrieve data
		} catch (final ServiceException e) {
			e.printStackTrace();
			logger.error("Consultar Habilitacoes error: ", e);
			serviceOutput = new ConsultaHabilitacoesServiceOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessages()));
		} catch (final Exception e) {
			e.printStackTrace();
			logger.error("Consultar Habilitacoes error: ", e);
			serviceOutput = new ConsultaHabilitacoesServiceOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessage()));
		}

		return serviceOutput;
	}

	@WebMethod(operationName = "listarNacionalidades")
	@WebResult(name = "output", targetNamespace = "http://msb.mozabanco.co.mz")
	public ConsultaNacionalidadesOutput consultaNacionalidades(
			@WebParam(name = "input", targetNamespace = "http://msb.mozabanco.co.mz") ServiceInput input) {

		ConsultaNacionalidadesOutput serviceOutput = null;

		try {
			logger.debug("ConsultaHRSap input:" + input);

			// create provider
			final SapHrProvider hrProvider = new SapHrProvider();
			// retrieve data
			serviceOutput = hrProvider.findNacionalidades(input);
			// retrieve data
		} catch (final ServiceException e) {
			e.printStackTrace();
			logger.error("Consultar Nacionalidades error", e);
			serviceOutput = new ConsultaNacionalidadesOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessages()));
		} catch (final Exception e) {
			e.printStackTrace();
			logger.error("Consultar Nacionalidades error", e);
			serviceOutput = new ConsultaNacionalidadesOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessage()));
		}

		return serviceOutput;
	}

	@WebMethod(operationName = "listarAusenciasColaborador")
	@WebResult(name = "output", targetNamespace = "http://msb.mozabanco.co.mz")
	public ConsultaAusenciasOutput consultaAusenciasColaborador(
			@WebParam(name = "input", targetNamespace = "http://msb.mozabanco.co.mz") ConsultaAusenciasInput input) {

		ConsultaAusenciasOutput serviceOutput = null;

		try {
			logger.debug("Consultar Ausencias Colaborador input:" + input);

			final SapHrProvider hrProvider = new SapHrProvider();

			serviceOutput = hrProvider.findAusencias(input);
			logger.debug("Consultar Ausencias Colaborador output:" + serviceOutput);

		} catch (final ServiceException e) {
			e.printStackTrace();
			logger.error("Consultar Ausencias Colaborador error: ", e);
			serviceOutput = new ConsultaAusenciasOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessages()));
		} catch (final Exception e) {
			e.printStackTrace();
			logger.error("Consultar Ausencias Colaborador error: ", e);
			serviceOutput = new ConsultaAusenciasOutput();
			serviceOutput.setStatus(StatusValidator.getErrorStatus(e
					.getMessage()));
		}

		return serviceOutput;
	}
}