package com.moza.msb.util;

import com.moza.msb.backend.xmlmapping.SapColaborador;
import com.moza.msb.bean.Colaborador;

import java.util.ArrayList;
import java.util.List;

public class ProviderUtil {

	public static List<Colaborador> mapColab(List<SapColaborador> colabs) {
		List<Colaborador> colaboradors = new ArrayList<>();
		for (SapColaborador colab : colabs) {
			Colaborador colaborador = new Colaborador();
			colaborador.setApelido(colab.getApelido());
			colaborador.setAreaCurso(colab.getAreacurso());
			// colaborador.setArquivoDoc(arquivoDoc);
			colaborador.setNumeroDoc(colab.getNumerobi());
			colaborador.setNuit(colab.getNuit());
			colaborador.setDataAdmissao(colab.getDataadmissao());
			colaborador.setCodigoCategoria(colab.getCodigocategoria());
			colaborador.setCodigoPostal(colab.getCodigopostal());
			colaborador.setCodigoUnidadeOrganica(colab.getUnidadeorganiz());
			colaborador.setDataEmissao(colab.getDataemissao());
			colaborador.setDataFimContrato(colab.getDatafimcontrato());
			colaborador.setDataNascimento(colab.getDatanascimento());
			colaborador.setDataValidadeDoc(colab.getDataValBi());
			colaborador.setDescricaoCategoria(colab.getDescricaocategoria());
			colaborador.setEmail(colab.getEmail());
			colaborador.setEndereco(colab.getEndereco());
			colaborador.setEstadoCivil(colab.getEstadocivil());
			colaborador.setFuncao(colab.getFuncao());
			colaborador.setHabilitacaoLiteraria(colab.getHabilitacaoliteraria());
			colaborador.setInstituicaoEnsino(colab.getInstituicaoensino());
			colaborador.setLocalidade(colab.getLocalidade());
			colaborador.setLocalNascimento(colab.getLocalnascimento());
			colaborador.setNacionalidade(colab.getNacionalidade());
			colaborador.setNiss(colab.getNiss());
			colaborador.setNomeCompleto(colab.getNomecompleto());
			colaborador.setNumeroEmpregado(colab.getNumeroempregado());
			colaborador.setPrimeiroNome(colab.getPrimeironome());
			colaborador.setSexo(colab.getSexo());
			colaborador.setSituacao(colab.getSituacao());
			colaborador.setTelefone(colab.getTelefone());
			colaborador.setTelemovel(colab.getTelemovel());
			colaborador.setUniversidade(colab.getUniversidade());
			colaborador.setAreaCursoId(colab.getAreaCursoId());
			colaboradors.add(colaborador);
		}
		return colaboradors;
	}
}
