package com.moza.msb.provider;

import com.moza.msb.common.beans.ServiceInput;
import com.moza.msb.common.exceptions.ServiceException;
import com.moza.msb.backend.executors.SapRhHttpRequestExecutor;
import com.moza.msb.backend.xmlmapping.*;
import com.moza.msb.bean.*;
import com.moza.msb.io.*;
import com.moza.msb.util.ProviderUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;


public class SapHrProvider {
	private static Logger logger = Logger.getLogger(SapHrProvider.class);
	private SapRhHttpRequestExecutor httpRequest;

	public SapHrProvider() {
		httpRequest = new SapRhHttpRequestExecutor();
		// TODO Auto-generated method stub
	}

	public List<Colaborador> findColaborador(ConsultaColaboradoresInput input) throws ServiceException {
		if (StringUtils.isEmpty(input.getNumColaborador())) {
			input.setNumColaborador("");
		}
		return ProviderUtil.mapColab(this.httpRequest.consultaColaborador(input.getNumColaborador()));
	}

	public List<Categoria> consultaCategoria(ConsultaCategoriaInput input) throws ServiceException {
		List<Categoria> categorias = new ArrayList<Categoria>();
		// TODO Auto-generated method stub
		for (SapCategoria cc : this.httpRequest.consultarCategoria().getCategorias()) {
			Categoria categoria = new Categoria();
			categoria.setCodigoCategoria(cc.getCodigocategoria());
			categoria.setNome(cc.getNome());
			categorias.add(categoria);
		}
		return categorias;
	}

	public List<Posicao> consultaPosicao(ConsultaPosicaoInput input) throws ServiceException {

		List<Posicao> cpList = new ArrayList<Posicao>();
		for (SapPosicao cc : this.httpRequest.consultarPosicao().getPosicoes()) {
			Posicao consultaPosicao = new Posicao();
			consultaPosicao.setCodigoPosicao(cc.getCodigoPosicao());
			consultaPosicao.setNome(cc.getNome());
			cpList.add(consultaPosicao);
		}
		return cpList;
	}

	public ConsultaHabilitacoesServiceOutput getHabilitacoes(ServiceInput input) throws ServiceException {
		final ConsultaHabilitacoesServiceOutput output = new ConsultaHabilitacoesServiceOutput();
		final ArrayList<Habilitacao> habilitacoes = new ArrayList<Habilitacao>();
		final List<SapHabilitacao> habs = httpRequest.consultaHabilitacoes();
		for (final SapHabilitacao hab : habs) {
			final Habilitacao habilitacao = new Habilitacao();
			habilitacao.setDescricao(hab.getDescricao());
			habilitacao.setNome(hab.getNome());
			habilitacao.setIdHabilitacao(hab.getIdhabilitacao());
			habilitacoes.add(habilitacao);
		}
		output.setHabilitacoes(new Habilitacoes(habilitacoes));
		return output;
	}

	public ConsultaUnidadeOrganicaOutput findUnidadesOrganicas(ConsultaUnidadeOrganicaInput input)
			throws ServiceException {
		final ConsultaUnidadeOrganicaOutput output = new ConsultaUnidadeOrganicaOutput();
		final ArrayList<UnidadeOrganica> unidadesOrganicas = new ArrayList<UnidadeOrganica>();
		final List<SapUnidadeOrganica> uns = httpRequest.consultaUnidadesOrganicas();
		for (final SapUnidadeOrganica un : uns) {
			final UnidadeOrganica unidade = new UnidadeOrganica();
			unidade.setCidade(un.getCidade());
			unidade.setCodigo(un.getCodigo());
			unidade.setCodigoPostal(un.getCodigopostal());
			unidade.setEmail(un.getEmail());
			unidade.setFax(un.getFax());
			unidade.setMorada(un.getMorada());
			unidade.setNome(un.getNome());
			unidade.setTelefone(un.getTelefone());
			unidade.setResponsavelUnidade(un.getResponsunidade());
			unidade.setUnidadeSuperior(un.getUnidadesuperior());
			unidadesOrganicas.add(unidade);
		}
		output.setUnidadesOrganicas(new UnidadesOrganicas(unidadesOrganicas));
		return output;
	}

	public ConsultaNacionalidadesOutput findNacionalidades(ServiceInput input) throws ServiceException {
		final ConsultaNacionalidadesOutput output = new ConsultaNacionalidadesOutput();
		final ArrayList<Nacionalidade> nacionalidades = new ArrayList<Nacionalidade>();
		final List<SapNacionalidade> nacs = httpRequest.consultaNacionalidades();
		logger.debug("Result Size: " + nacs.size());
		for (final SapNacionalidade nac : nacs) {
			final Nacionalidade nacionalidade = new Nacionalidade();

			nacionalidade.setDescricao(nac.getDescricao());
			nacionalidade.setNome(nac.getNome());
			nacionalidade.setIdNacionalidade(nac.getIdhabilitacao());
			nacionalidades.add(nacionalidade);
		}
		output.setNacionalidades(new Nacionalidades(nacionalidades));
		return output;
	}

	public ConsultaAusenciasOutput findAusencias(ConsultaAusenciasInput input) throws ServiceException {
		final ConsultaAusenciasOutput output = new ConsultaAusenciasOutput();
		final ArrayList<Ausencia> ferias = new ArrayList<Ausencia>();
		final List<SapFerias> nacs = httpRequest.consultaFerias(input.getAusencia(), input.getDataInicio(),
				input.getNumColaborador());
		logger.debug("Result Size: " + nacs.size());
		for (final SapFerias nac : nacs) {
			final Ausencia fer = new Ausencia();
			fer.setData(nac.getData());
			fer.setDuracao(nac.getDuracao());
			fer.setHoraFim(nac.getHoraFim());
			fer.setHoraInicio(nac.getHoraInicio());
			fer.setNomeAusencia(nac.getNomeAusencia());
			fer.setNumeroEmpregado(nac.getNumeroEmpregado());
			ferias.add(fer);
		}
		output.setAusencias(new Ausencias(ferias));
		return output;
	}
}
