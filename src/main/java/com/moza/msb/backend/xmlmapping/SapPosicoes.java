package com.moza.msb.backend.xmlmapping;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MtRespConsultaposicoes")
public class SapPosicoes {
	@XStreamImplicit(itemFieldName = "item")
	private List<SapPosicao> posicao;

	public List<SapPosicao> getPosicoes() {
		return posicao;
	}

	public void setPosicoes(List<SapPosicao> posicao) {
		this.posicao = posicao;
	}
}
