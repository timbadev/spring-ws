package com.moza.msb.backend.xmlmapping;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class SapHabilitacao {

	@XStreamAlias("Descricao")
	private String descricao;
	@XStreamAlias("Nome")
	private String nome;
	@XStreamAlias("Idhabilitacao")
	private String idhabilitacao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIdhabilitacao() {
		return idhabilitacao;
	}

	public void setIdhabilitacao(String idhabilitacao) {
		this.idhabilitacao = idhabilitacao;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SapHabilitacao [descricao=");
		builder.append(descricao);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", idhabilitacao=");
		builder.append(idhabilitacao);
		builder.append("]");
		return builder.toString();
	}

}
