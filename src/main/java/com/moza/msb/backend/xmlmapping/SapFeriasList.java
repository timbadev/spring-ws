package com.moza.msb.backend.xmlmapping;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MtRespConsultaferias")
public class SapFeriasList {

	@XStreamImplicit(itemFieldName = "item")
	private List<SapFerias> ferias;

	public List<SapFerias> getFerias() {
		return ferias;
	}

	public void setFerias(List<SapFerias> ferias) {
		this.ferias = ferias;
	}

}
