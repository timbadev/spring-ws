package com.moza.msb.backend.xmlmapping;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MtRespConsultanacionaldiades")
public class SapNacionalidades {

	@XStreamImplicit(itemFieldName = "item")
	List<SapNacionalidade> nacionalidades;

	public List<SapNacionalidade> getNacionalidades() {
		return nacionalidades;
	}

	public void setNacionalidades(List<SapNacionalidade> nacionalidades) {
		this.nacionalidades = nacionalidades;
	}
}
