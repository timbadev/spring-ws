package com.moza.msb.backend.xmlmapping;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MtRespConsultahabilitacoes")
public class SapHabilitacoes {

	@XStreamImplicit(itemFieldName = "item")
	private List<SapHabilitacao> habilitacoes;

	public List<SapHabilitacao> getHabilitacoes() {
		return habilitacoes;
	}

	public void setHabilitacoes(List<SapHabilitacao> habilitacoes) {
		this.habilitacoes = habilitacoes;
	}

}
