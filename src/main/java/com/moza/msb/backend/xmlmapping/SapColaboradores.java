package com.moza.msb.backend.xmlmapping;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MtRespConsultacolaborador")
public class SapColaboradores {
	
	@XStreamImplicit(itemFieldName = "item")
	private List<SapColaborador> colaboradores;

	public List<SapColaborador> getColaboradores() {
		return colaboradores;
	}

	public void setColaboradores(List<SapColaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}

}
