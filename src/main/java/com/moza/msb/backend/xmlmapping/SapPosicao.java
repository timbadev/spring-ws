package com.moza.msb.backend.xmlmapping;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class SapPosicao {
	@XStreamAlias("Codigoposicao")
	public String codigoPosicao;
	
	@XStreamAlias("Nome")
	public String nome;

	public String getCodigoPosicao() {
		return codigoPosicao;
	}

	public void setCodigoPosicao(String codigoPosicao) {
		this.codigoPosicao = codigoPosicao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Posicao [codigoPosicao=" + codigoPosicao + ", nome=" + nome + "]";
	}
}
