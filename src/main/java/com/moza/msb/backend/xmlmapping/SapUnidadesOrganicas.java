package com.moza.msb.backend.xmlmapping;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MtRespConsultaunidadesorgani")
public class SapUnidadesOrganicas {

	@XStreamImplicit(itemFieldName = "item")
	private List<SapUnidadeOrganica> unidades;

	public List<SapUnidadeOrganica> getUnidades() {
		return unidades;
	}

	public void setUnidades(List<SapUnidadeOrganica> unidades) {
		this.unidades = unidades;
	}
}
