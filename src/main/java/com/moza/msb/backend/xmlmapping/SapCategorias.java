package com.moza.msb.backend.xmlmapping;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("MtRespConsultacategoria")
public class SapCategorias {
	@XStreamImplicit(itemFieldName = "item")
	private List<SapCategoria> categoria;

	public List<SapCategoria> getCategorias() {
		return categoria;
	}

	public void setCategorias(List<SapCategoria> categoria) {
		this.categoria = categoria;
	}
}
