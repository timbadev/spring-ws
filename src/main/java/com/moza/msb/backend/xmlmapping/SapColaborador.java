package com.moza.msb.backend.xmlmapping;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class SapColaborador {
	
	@XStreamAlias("Codigoposicao")
	private String codigoPosicao;
	
	@XStreamAlias("Apelido")
	private String apelido;

	@XStreamAlias("Areacurso")
	private String areacurso;

	@XStreamAlias("Codigocategoria")
	private String codigocategoria;

	@XStreamAlias("Codigopostal")
	private String codigopostal;

	@XStreamAlias("DataValBi")
	private String datavalbi;

	@XStreamAlias("Dataadmissao")
	private String dataadmissao;

	@XStreamAlias("Dataemissao")
	private String dataemissao;

	@XStreamAlias("Datafimcontrato")
	private String datafimcontrato;

	@XStreamAlias("Datanascimento")
	private String datanascimento;

	@XStreamAlias("Descricaocategoria")
	private String descricaocategoria;

	@XStreamAlias("Email")
	private String email;

	@XStreamAlias("Endereco")
	private String endereco;

	@XStreamAlias("Estadocivil")
	private String estadocivil;

	@XStreamAlias("Funcao")
	private String funcao;

	@XStreamAlias("Habilitacaoliteraria")
	private String habilitacaoliteraria;

	@XStreamAlias("Instituicaoensino")
	private String instituicaoensino;

	@XStreamAlias("Localnascimento")
	private String localnascimento;

	@XStreamAlias("Localidade")
	private String localidade;

	@XStreamAlias("Nacionalidade")
	private String nacionalidade;

	@XStreamAlias("Niss")
	private String niss;

	@XStreamAlias("Nomecompleto")
	private String nomecompleto;

	@XStreamAlias("Nuit")
	private String nuit;

	@XStreamAlias("Numeroempregado")
	private String numeroempregado;

	@XStreamAlias("Primeironome")
	private String primeironome;

	@XStreamAlias("Sexo")
	private String sexo;

	@XStreamAlias("Situacao")
	private String situacao;

	@XStreamAlias("Telefone")
	private String telefone;

	@XStreamAlias("Telemovel")
	private String telemovel;

	@XStreamAlias("Universidade")
	private String universidade;

	@XStreamAlias("Numerobi")
	private String numerobi;

	@XStreamAlias("Unidadeorganiz")
	private String unidadeorganiz;

	@XStreamAlias("Areacursoid")
	private String areaCursoId;
	
	@XStreamAlias("Descricaoposicao")
	private String descricaoPosicao;
	

	public String getDescricaoposicao() {
		return this.descricaoPosicao;
	}

	public void setDescricaoposicao(String descricaoposicao) {
		this.descricaoPosicao = descricaoposicao;
	}

	public String getAreaCursoId() {
		return areaCursoId;
	}

	public void setAreaCursoId(String areaCursoId) {
		this.areaCursoId = areaCursoId;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getAreacurso() {
		return areacurso;
	}

	public void setAreacurso(String areacurso) {
		this.areacurso = areacurso;
	}

	public String getCodigocategoria() {
		return codigocategoria;
	}

	public void setCodigocategoria(String codigocategoria) {
		this.codigocategoria = codigocategoria;
	}

	public String getCodigopostal() {
		return codigopostal;
	}

	public void setCodigopostal(String codigopostal) {
		this.codigopostal = codigopostal;
	}

	public String getDataValBi() {
		return datavalbi;
	}

	public void setDataValBi(String dataValBi) {
		this.datavalbi = dataValBi;
	}

	public String getDataadmissao() {
		return dataadmissao;
	}

	public void setDataadmissao(String dataadmissao) {
		this.dataadmissao = dataadmissao;
	}

	public String getDataemissao() {
		return dataemissao;
	}

	public void setDataemissao(String dataemissao) {
		this.dataemissao = dataemissao;
	}

	public String getDatafimcontrato() {
		return datafimcontrato;
	}

	public void setDatafimcontrato(String datafimcontrato) {
		this.datafimcontrato = datafimcontrato;
	}

	public String getDatanascimento() {
		return datanascimento;
	}

	public void setDatanascimento(String datanascimento) {
		this.datanascimento = datanascimento;
	}

	public String getDescricaocategoria() {
		return descricaocategoria;
	}

	public void setDescricaocategoria(String descricaocategoria) {
		this.descricaocategoria = descricaocategoria;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEstadocivil() {
		return estadocivil;
	}

	public void setEstadocivil(String estadocivil) {
		this.estadocivil = estadocivil;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getHabilitacaoliteraria() {
		return habilitacaoliteraria;
	}

	public void setHabilitacaoliteraria(String habilitacaoliteraria) {
		this.habilitacaoliteraria = habilitacaoliteraria;
	}

	public String getInstituicaoensino() {
		return instituicaoensino;
	}

	public void setInstituicaoensino(String instituicaoensino) {
		this.instituicaoensino = instituicaoensino;
	}

	public String getLocalnascimento() {
		return localnascimento;
	}

	public void setLocalnascimento(String localnascimento) {
		this.localnascimento = localnascimento;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getNiss() {
		return niss;
	}

	public void setNiss(String niss) {
		this.niss = niss;
	}

	public String getNomecompleto() {
		return nomecompleto;
	}

	public void setNomecompleto(String nomecompleto) {
		this.nomecompleto = nomecompleto;
	}

	public String getNuit() {
		return nuit;
	}

	public void setNuit(String nuit) {
		this.nuit = nuit;
	}

	public String getNumeroempregado() {
		return numeroempregado;
	}

	public void setNumeroempregado(String numeroempregado) {
		this.numeroempregado = numeroempregado;
	}

	public String getPrimeironome() {
		return primeironome;
	}

	public void setPrimeironome(String primeironome) {
		this.primeironome = primeironome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getTelemovel() {
		return telemovel;
	}

	public void setTelemovel(String telemovel) {
		this.telemovel = telemovel;
	}

	public String getUniversidade() {
		return universidade;
	}

	public void setUniversidade(String universidade) {
		this.universidade = universidade;
	}

	public String getNumerobi() {
		return numerobi;
	}

	public void setNumerobi(String numerobi) {
		this.numerobi = numerobi;
	}

	public String getUnidadeorganiz() {
		return unidadeorganiz;
	}

	public void setUnidadeorganiz(String unidadeorganiz) {
		this.unidadeorganiz = unidadeorganiz;
	}

	public String getCodigoPosicao() {
		return codigoPosicao;
	}

	public void setCodigoPosicao(String codigoPosicao) {
		this.codigoPosicao = codigoPosicao;
	}

	public String getDatavalbi() {
		return datavalbi;
	}

	public void setDatavalbi(String datavalbi) {
		this.datavalbi = datavalbi;
	}

	public String getDescricaoPosicao() {
		return descricaoPosicao;
	}

	public void setDescricaoPosicao(String descricaoPosicao) {
		this.descricaoPosicao = descricaoPosicao;
	}

	@Override
	public String toString() {
		return "SapColaborador [codigoPosicao=" + codigoPosicao + ", apelido=" + apelido + ", areacurso=" + areacurso
				+ ", codigocategoria=" + codigocategoria + ", codigopostal=" + codigopostal + ", datavalbi=" + datavalbi
				+ ", dataadmissao=" + dataadmissao + ", dataemissao=" + dataemissao + ", datafimcontrato="
				+ datafimcontrato + ", datanascimento=" + datanascimento + ", descricaocategoria=" + descricaocategoria
				+ ", email=" + email + ", endereco=" + endereco + ", estadocivil=" + estadocivil + ", funcao=" + funcao
				+ ", habilitacaoliteraria=" + habilitacaoliteraria + ", instituicaoensino=" + instituicaoensino
				+ ", localnascimento=" + localnascimento + ", localidade=" + localidade + ", nacionalidade="
				+ nacionalidade + ", niss=" + niss + ", nomecompleto=" + nomecompleto + ", nuit=" + nuit
				+ ", numeroempregado=" + numeroempregado + ", primeironome=" + primeironome + ", sexo=" + sexo
				+ ", situacao=" + situacao + ", telefone=" + telefone + ", telemovel=" + telemovel + ", universidade="
				+ universidade + ", numerobi=" + numerobi + ", unidadeorganiz=" + unidadeorganiz + ", areaCursoId="
				+ areaCursoId + ", descricaoPosicao=" + descricaoPosicao + "]";
	}
}
