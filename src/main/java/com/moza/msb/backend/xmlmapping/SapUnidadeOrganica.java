package com.moza.msb.backend.xmlmapping;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class SapUnidadeOrganica {
	@XStreamAlias("Cidade")
	private String cidade;
	@XStreamAlias("Codigo")
	private String codigo;
	@XStreamAlias("Codigopostal")
	private String codigopostal;
	@XStreamAlias("Email")
	private String email;
	@XStreamAlias("Fax")
	private String fax;
	@XStreamAlias("Morada")
	private String morada;
	@XStreamAlias("Nome")
	private String nome;
	@XStreamAlias("Telefone")
	private String telefone;
	@XStreamAlias("Unidadesuperior")
	private String unidadesuperior;
	@XStreamAlias("Responsunidade")
	private String responsunidade;

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigopostal() {
		return codigopostal;
	}

	public void setCodigopostal(String codigopostal) {
		this.codigopostal = codigopostal;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMorada() {
		return morada;
	}

	public void setMorada(String morada) {
		this.morada = morada;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getUnidadesuperior() {
		return unidadesuperior;
	}

	public void setUnidadesuperior(String unidadesuperior) {
		this.unidadesuperior = unidadesuperior;
	}

	public String getResponsunidade() {
		return responsunidade;
	}

	public void setResponsunidade(String responsunidade) {
		this.responsunidade = responsunidade;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SapUnidadeOrganica [cidade=");
		builder.append(cidade);
		builder.append(", codigo=");
		builder.append(codigo);
		builder.append(", codigopostal=");
		builder.append(codigopostal);
		builder.append(", email=");
		builder.append(email);
		builder.append(", fax=");
		builder.append(fax);
		builder.append(", morada=");
		builder.append(morada);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", telefone=");
		builder.append(telefone);
		builder.append(", unidadesuperior=");
		builder.append(unidadesuperior);
		builder.append(", responsunidade=");
		builder.append(responsunidade);
		builder.append("]");
		return builder.toString();
	}

}
