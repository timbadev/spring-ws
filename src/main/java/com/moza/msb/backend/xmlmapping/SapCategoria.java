package com.moza.msb.backend.xmlmapping;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class SapCategoria {
	
	@XStreamAlias("Codigocategoria")
	public String codigoCategoria;
	
	@XStreamAlias("Nome")
	public String nome;
	
	public String getCodigocategoria() {
		return codigoCategoria;
	}
	public void setCodigocategoria(String codigocategoria) {
		this.codigoCategoria = codigocategoria;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "Categoria [codigoCategoria=" + codigoCategoria + ", nome=" + nome + "]";
	}
}
