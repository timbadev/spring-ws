package com.moza.msb.backend.xmlmapping;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class SapFerias {

	@XStreamAlias("Numeroempregado")
	private String numeroEmpregado;
	
	@XStreamAlias("Nomeausencia")
	private String nomeAusencia;

	@XStreamAlias("Data")
	private String data;
	
	@XStreamAlias("HoraInicio")
	private String horaInicio;
	
	@XStreamAlias("HoraFim")
	private String horaFim;
	
	@XStreamAlias("Duracao")
	private String duracao;

	public String getNumeroEmpregado() {
		return numeroEmpregado;
	}

	public void setNumeroEmpregado(String numeroEmpregado) {
		this.numeroEmpregado = numeroEmpregado;
	}

	public String getNomeAusencia() {
		return nomeAusencia;
	}

	public void setNomeAusencia(String nomeAusencia) {
		this.nomeAusencia = nomeAusencia;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public String getDuracao() {
		return duracao;
	}

	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}
	
	
}
