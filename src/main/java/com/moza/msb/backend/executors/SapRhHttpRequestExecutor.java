package com.moza.msb.backend.executors;

import com.moza.msb.common.exceptions.ServiceException;
import com.moza.msb.common.utils.Convert;
import com.moza.msb.common.utils.XMLParser;
import com.moza.msb.backend.xmlmapping.*;
import com.thoughtworks.xstream.XStream;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class SapRhHttpRequestExecutor {

	private static Logger logger = Logger.getLogger(SapRhHttpRequestExecutor.class);

	private SapHttpExecutor executor;
	private static final String rhServiceUrl = "http://SVDCPAPQ23.mozabanco.co.mz:8000/sap/bc/srt/rfc/sap/zws_sgbl/100/zws_sgbl/zws_sgbl";
	private static final String pass = "moza!ws14";
	private static final String username = "webservice";

	public SapRhHttpRequestExecutor() {
		this.executor = new SapHttpExecutor();
	}

	private String execute(String action, String request) throws ServiceException {

		return this.executor.execute(username, pass, rhServiceUrl, action, request);
	}

	public List<SapColaborador> consultaColaborador(String numColaborador) {

		List<SapColaborador> colaboradores = null;

		String soapXmlRequest = new StringBuilder()
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">")
				.append("<soapenv:Header/>")
				.append("<soapenv:Body>")
				.append("<urn:ZhrConsultacolaborador>")
				.append("<NumColaborador>")
				.append(numColaborador)
				.append("</NumColaborador>")
				.append("</urn:ZhrConsultacolaborador>")
				.append("</soapenv:Body>")
				.append("</soapenv:Envelope>")
				.toString();
		try {
			String response = this.execute("urn:ZhrConsultacolaborador", soapXmlRequest);
			logger.debug("Resposta: " + response);
			XStream xstream = new XStream();

			xstream.processAnnotations(SapColaboradores.class);
			xstream.processAnnotations(SapColaborador.class);

			String responseObject = XMLParser.findTag("n0:ZhrConsultacolaboradorResponse", response);

			SapColaboradores colabs = (SapColaboradores) xstream.fromXML(responseObject);
			colaboradores = colabs.getColaboradores();

		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return colaboradores;
	}

	public List<SapHabilitacao> consultaHabilitacoes() {

		List<SapHabilitacao> habilitacoes = null;

		String soapXmlRequest = new StringBuilder()
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">")
				.append("<soapenv:Header/>").append("<soapenv:Body>").append(" <urn:ZhrConsultahabilitacoes/>")
				.append("</soapenv:Body>").append("</soapenv:Envelope>").toString();

		String requestString = Convert.toUTF8(soapXmlRequest);

		try {
			String response = this.execute("urn:ZhrConsultahabilitacoes", requestString);

			XStream xstream = new XStream();

			xstream.processAnnotations(SapHabilitacao.class);

			xstream.processAnnotations(SapHabilitacoes.class);

			String responseObject = XMLParser.findTag("n0:ZhrConsultahabilitacoesResponse", response);

			SapHabilitacoes colabs = (SapHabilitacoes) xstream.fromXML(responseObject);
			habilitacoes = colabs.getHabilitacoes();

		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return habilitacoes;
	}

	public List<SapNacionalidade> consultaNacionalidades() {

		List<SapNacionalidade> nacionalidades = null;

		String soapXmlRequest = new StringBuilder()
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">")
				.append("<soapenv:Header/>").append("<soapenv:Body>").append(" <urn:ZhrConsultanacionaldiades/>")
				.append("</soapenv:Body>").append("</soapenv:Envelope>").toString();
		try {
			String response = this.execute("urn:ZhrConsultanacionaldiades", soapXmlRequest);
			XStream xstream = new XStream();

			xstream.processAnnotations(SapNacionalidade.class);
			xstream.processAnnotations(SapNacionalidades.class);

			String responseObject = XMLParser.findTag("n0:ZhrConsultanacionaldiadesResponse", response);
			logger.debug(responseObject);
			SapNacionalidades colabs = (SapNacionalidades) xstream.fromXML(responseObject);
			nacionalidades = colabs.getNacionalidades();

		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return nacionalidades;
	}

	public List<SapUnidadeOrganica> consultaUnidadesOrganicas() {

		List<SapUnidadeOrganica> unidadesOrganicas = null;

		String soapXmlRequest = new StringBuilder()
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">")
				.append("<soapenv:Header/>").append("<soapenv:Body>").append(" <urn:ZhrConsultaunidadesorganicas/>")
				.append("</soapenv:Body>").append("</soapenv:Envelope>").toString();
		try {
			String response = this.execute("urn:ZhrConsultaunidadesorganicas", soapXmlRequest);
			XStream xstream = new XStream();

			xstream.processAnnotations(SapUnidadeOrganica.class);
			xstream.processAnnotations(SapUnidadesOrganicas.class);

			String responseObject = XMLParser.findTag("n0:ZhrConsultaunidadesorganicasResponse", response);

			SapUnidadesOrganicas colabs = (SapUnidadesOrganicas) xstream.fromXML(responseObject);
			unidadesOrganicas = colabs.getUnidades();

		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return unidadesOrganicas;
	}

	public List<SapFerias> consultaFerias(String ausencia, String dataInicio, String numColaborador) {

		List<SapFerias> ferias = new ArrayList<>();

		String soapXmlRequest = new StringBuilder()
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">")
				.append("<soapenv:Header/>").append("<soapenv:Body>").append(" <urn:ZhrConsultaferiascolaborador>")
				.append("<ICodAusencia>" + ausencia + "</ICodAusencia>")
				.append("<IDataInicio>" + dataInicio + "</IDataInicio>")
				.append("<INumColaborador>" + numColaborador + "</INumColaborador>")
				.append("</urn:ZhrConsultaferiascolaborador>").append("</soapenv:Body>").append("</soapenv:Envelope>")
				.toString();

		try {
			String response = this.execute("urn:ZhrConsultaferiascolaboradors", soapXmlRequest);
			XStream xstream = new XStream();

			xstream.processAnnotations(SapFerias.class);
			xstream.processAnnotations(SapFeriasList.class);

			String responseObject = XMLParser.findTag("n0:ZhrConsultaferiascolaboradorResponse", response);

			SapFeriasList feriasList = (SapFeriasList) xstream.fromXML(responseObject);
			ferias = feriasList.getFerias();

		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return ferias;
	}

	public SapCategorias consultarCategoria() {
		// TODO Auto-generated method stub
		SapCategorias categorias = null;

		String soapXmlRequest = new StringBuilder()
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">")
				.append("<soapenv:Header/>").append("<soapenv:Body>").append("<urn:ZhrConsultacategoria/>")
				.append("</soapenv:Body>").append("</soapenv:Envelope>").toString();

		try {
			String response = this.execute("urn:ZhrConsultacategoria", soapXmlRequest);
			XStream xstream = new XStream();

			xstream.processAnnotations(SapCategorias.class);
			xstream.processAnnotations(SapCategoria.class);

			String responseObject = XMLParser.findTag("n0:ZhrConsultacategoriaResponse", response);

			categorias = (SapCategorias) xstream.fromXML(responseObject);

		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return categorias;
	}

	public SapPosicoes consultarPosicao() {
		// TODO Auto-generated method stub
		SapPosicoes posicoes = null;

		String soapXmlRequest = new StringBuilder()
				.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">")
				.append("<soapenv:Header/>").append("<soapenv:Body>").append("<urn:ZhrConsultarposicao/>")
				.append("</soapenv:Body>").append("</soapenv:Envelope>").toString();

		try {
			String response = this.execute("urn:ZhrConsultarposicao", soapXmlRequest);
			XStream xstream = new XStream();

			xstream.processAnnotations(SapPosicoes.class);
			xstream.processAnnotations(SapPosicao.class);

			String responseObject = XMLParser.findTag("n0:ZhrConsultarposicaoResponse", response);

			posicoes = (SapPosicoes) xstream.fromXML(responseObject);

		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return posicoes;
	}

}
