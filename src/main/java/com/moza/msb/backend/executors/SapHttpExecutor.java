package com.moza.msb.backend.executors;

import com.moza.msb.common.exceptions.ServiceException;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;


public class SapHttpExecutor {

	private static Logger logger = Logger.getLogger(SapHttpExecutor.class);

	public String execute(String username, String passsword,
			String rhServiceUrl, String action, String request)
			throws ServiceException {

		String responseString = "";

		try {

			boolean debugEnabled = logger.isDebugEnabled();

			if (debugEnabled) { 
				logger.debug("HttpRequestExecutor " + rhServiceUrl + " action:"
						+ action + " request:\n" + request);
			}

			int responsestatus = HttpURLConnection.HTTP_OK;

			try {
				byte[] soapbytes = request.getBytes("UTF-8");

				URL url = new URL(rhServiceUrl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestProperty("Content-Length",
						String.valueOf(soapbytes.length));
				conn.setRequestProperty("Content-type",
						"text/xml; charset=utf-8");
				conn.setRequestProperty("Connection", "Keep-Alive");

				String userPassword = username + ":" + passsword;

				String encoding = Base64.getEncoder().encodeToString(userPassword
						.getBytes());
                System.out.println(encoding);
				conn.setRequestProperty("Authorization", "Basic " + encoding);

				conn.setRequestMethod("POST");
				conn.setDoOutput(true);
				conn.setDoInput(true);
				conn.setRequestProperty("SOAPAction", action);

				// write data
				OutputStream reqStream = conn.getOutputStream();
				reqStream.write(soapbytes);
				reqStream.close();

				if (debugEnabled) {
					// Send the request and print the reply
					logger.debug("Response code: " + conn.getResponseCode());
					logger.debug("Response msg: " + conn.getResponseMessage());
				}

				if (conn.getResponseCode() == 500) {

					if (debugEnabled) {
						logger.debug("500 error length: "
								+ conn.getContentLength());
					}

					/* error from server */
					InputStream in = conn.getErrorStream();

					if (debugEnabled) {
						logger.debug("Retrieving error: ");
					}

					BufferedReader reader = new BufferedReader(
							new InputStreamReader(in));

					StringBuffer responsebuffer = new StringBuffer();
					String responseline = null;

					while ((responseline = reader.readLine()) != null) {
						responsebuffer.append(responseline);
					}

					responseString = responsebuffer.toString();

					logger.error("HttpRequestExecutor error response ("
							+ responsestatus + ") " + responseString);

					throw new ServiceException(responseString);
				}

				responsestatus = conn.getResponseCode();
				InputStream in = conn.getInputStream();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(in));

				StringBuffer responsebuffer = new StringBuffer();
				String responseline = null;
				while ((responseline = reader.readLine()) != null) {
					// convert utf-8
					String UTF8Str = new String(responseline.getBytes(),
							"UTF-8");
					//
					responsebuffer.append(UTF8Str);
				}

				conn.disconnect();

				responseString = responsebuffer.toString();

			} catch (java.net.SocketTimeoutException e) {
				logger.error(e);
				throw new Exception(e.getMessage());
			} catch (java.io.IOException e) {
				logger.error(e);
				throw new Exception(e.getMessage());
			}

		} catch (Exception e) {
			logger.error("HttpRequestExecutor Exception: ", e);

			e.printStackTrace();

			throw new ServiceException(e.getMessage());
		}

		return responseString;
	}

}
