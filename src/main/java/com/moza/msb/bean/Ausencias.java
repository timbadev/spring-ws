package com.moza.msb.bean;

import java.util.ArrayList;

public class Ausencias {

	private ArrayList<Ausencia> ausencia;

	public Ausencias(){

	}

	public Ausencias(ArrayList<Ausencia> feriasList) {
		this.ausencia = feriasList;
	}


	public ArrayList<Ausencia> getAusencia() {
		return ausencia;
	}

	public void setAusencia(ArrayList<Ausencia> ausencia) {
		this.ausencia = ausencia;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Ausencias [ausencias=");
		builder.append(ausencia);
		builder.append("]");
		return builder.toString();
	}

}
