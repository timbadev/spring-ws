package com.moza.msb.bean;

import java.util.ArrayList;

public class FeriasDisponiveis {
	private ArrayList<PeriodoDisponivel> periodoDisponivel;

	public FeriasDisponiveis(ArrayList<PeriodoDisponivel> periodoDisponivel) {
		super();
		this.periodoDisponivel = periodoDisponivel;
	}

	public FeriasDisponiveis() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArrayList<PeriodoDisponivel> getPeriodoDisponivel() {
		return periodoDisponivel;
	}

	public void setPeriodoDisponivel(
			ArrayList<PeriodoDisponivel> periodoDisponivel) {
		this.periodoDisponivel = periodoDisponivel;
	}

}
