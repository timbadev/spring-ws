package com.moza.msb.bean;

public class PeriodoDisponivel {

	private String numColaborador;
	private String dataInicio;
	private String dataFim;
	private String dias;

	public String getNumColaborador() {
		return numColaborador; 
	}

	public void setNumColaborador(String numColaborador) {
		this.numColaborador = numColaborador;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public String getDias() {
		return dias;
	}

	public void setDias(String dias) {
		this.dias = dias;
	}
	

}
