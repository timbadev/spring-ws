package com.moza.msb.bean;

/**
 *
 */

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class Colaborador {
	private String numeroEmpregado;
	private String primeiroNome;
	private String apelido;
	private String nomeCompleto;
	private String dataNascimento;
	private String sexo;
	private String estadoCivil;
	private String areaCurso;
	private String instituicaoEnsino;
	private String endereco;
	private String localidade;
	private String telefone;
	private String nacionalidade;
	private String localNascimento;
	private String email;
	private String codigoCategoria;
	private String descricaoCategoria;
	private String dataEmissao;
	private String nuit;
	private String habilitacaoLiteraria;
	private String codigoPostal;
	private String telemovel;
	private String funcao;
	private String situacao;
	private String dataAdmissao;
	private String dataFimContrato;
	private String dataInicioProf;
	private String universidade;
	private String niss;
	private String dataValidadeDoc;
	private String numeroDoc;
	private String tipoDoc;
	private String descTipoDoc;
	private String arquivoDoc;
	private String codigoUnidadeOrganica;
	private String areaCursoId;

	public String getCodigoUnidadeOrganica() {
		return codigoUnidadeOrganica;
	}

	public void setCodigoUnidadeOrganica(String codigoUnidadeOrganica) {
		this.codigoUnidadeOrganica = codigoUnidadeOrganica;
	}

	public String getArquivoDoc() {
		return arquivoDoc;
	}

	public void setArquivoDoc(String arquivoDoc) {
		this.arquivoDoc = arquivoDoc;
	}

	public String getDataValidadeDoc() {
		return dataValidadeDoc;
	}

	public void setDataValidadeDoc(String dataValidadeDoc) {
		this.dataValidadeDoc = dataValidadeDoc;
	}

	public String getNumeroDoc() {
		return numeroDoc;
	}

	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getDescTipoDoc() {
		return descTipoDoc;
	}

	public void setDescTipoDoc(String descTipoDoc) {
		this.descTipoDoc = descTipoDoc;
	}

	public String getUniversidade() {
		return this.universidade;
	}

	public void setUniversidade(String universidade) {
		this.universidade = universidade;
	}

	public String getNiss() {
		return this.niss;
	}

	public void setNiss(String niss) {
		this.niss = niss;
	}

	public String getDataFimContrato() {
		return this.dataFimContrato;
	}

	public void setDataFimContrato(String dataFimContrato) {
		this.dataFimContrato = dataFimContrato;
	}

	public String getDataInicioProf() {
		return this.dataInicioProf;
	}

	public void setDataInicioProf(String dataInicioProf) {
		this.dataInicioProf = dataInicioProf;
	}

	public String getDataAdmissao() {
		return this.dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getSituacao() {
		return this.situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getFuncao() {
		return this.funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getTelemovel() {
		return this.telemovel;
	}

	public void setTelemovel(String telemovel) {
		this.telemovel = telemovel;
	}

	public String getNomeCompleto() {
		return this.nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getNumeroEmpregado() {
		return this.numeroEmpregado;
	}

	public String getDataNascimento() {
		return this.dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getAreaCurso() {
		return this.areaCurso;
	}

	public void setAreaCurso(String areaCurso) {
		this.areaCurso = areaCurso;
	}

	public String getInstituicaoEnsino() {
		return this.instituicaoEnsino;
	}

	public void setInstituicaoEnsino(String instituicaoEnsino) {
		this.instituicaoEnsino = instituicaoEnsino;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getLocalidade() {
		return this.localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getNacionalidade() {
		return this.nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getLocalNascimento() {
		return this.localNascimento;
	}

	public void setLocalNascimento(String localNascimento) {
		this.localNascimento = localNascimento;
	}

	public void setNumeroEmpregado(String numeroEmpregado) {
		this.numeroEmpregado = numeroEmpregado;
	}

	public String getPrimeiroNome() {
		return this.primeiroNome;
	}

	public void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}

	public String getApelido() {
		return this.apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodigoCategoria() {
		return this.codigoCategoria;
	}

	public void setCodigoCategoria(String codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}

	public String getDescricaoCategoria() {
		return this.descricaoCategoria;
	}

	public void setDescricaoCategoria(String descricaoCategoria) {
		this.descricaoCategoria = descricaoCategoria;
	}

	public String getDataEmissao() {
		return this.dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getNuit() {
		return nuit;
	}

	public void setNuit(String nuit) {
		this.nuit = nuit;
	}

	/**
	 * @return the habilitacaoLiteraria
	 */
	public String getHabilitacaoLiteraria() {
		return habilitacaoLiteraria;
	}

	/**
	 * @param habilitacaoLiteraria
	 *            the habilitacaoLiteraria to set
	 */
	public void setHabilitacaoLiteraria(String habilitacaoLiteraria) {
		this.habilitacaoLiteraria = habilitacaoLiteraria;
	}

	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}

	/**
	 * @param codigoPostal
	 *            the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getAreaCursoId() {
		return areaCursoId;
	}

	public void setAreaCursoId(String areaCursoId) {
		this.areaCursoId = areaCursoId;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Colaborador [numeroEmpregado=");
		builder.append(numeroEmpregado);
		builder.append(", primeiroNome=");
		builder.append(primeiroNome);
		builder.append(", apelido=");
		builder.append(apelido);
		builder.append(", nomeCompleto=");
		builder.append(nomeCompleto);
		builder.append(", dataNascimento=");
		builder.append(dataNascimento);
		builder.append(", sexo=");
		builder.append(sexo);
		builder.append(", estadoCivil=");
		builder.append(estadoCivil);
		builder.append(", areaCurso=");
		builder.append(areaCurso);
		builder.append(", instituicaoEnsino=");
		builder.append(instituicaoEnsino);
		builder.append(", endereco=");
		builder.append(endereco);
		builder.append(", localidade=");
		builder.append(localidade);
		builder.append(", telefone=");
		builder.append(telefone);
		builder.append(", nacionalidade=");
		builder.append(nacionalidade);
		builder.append(", localNascimento=");
		builder.append(localNascimento);
		builder.append(", email=");
		builder.append(email);
		builder.append(", codigoCategoria=");
		builder.append(codigoCategoria);
		builder.append(", descricaoCategoria=");
		builder.append(descricaoCategoria);
		builder.append(", dataEmissao=");
		builder.append(dataEmissao);
		builder.append(", nuit=");
		builder.append(nuit);
		builder.append(", habilitacaoLiteraria=");
		builder.append(habilitacaoLiteraria);
		builder.append(", codigoPostal=");
		builder.append(codigoPostal);
		builder.append(", telemovel=");
		builder.append(telemovel);
		builder.append(", funcao=");
		builder.append(funcao);
		builder.append(", situacao=");
		builder.append(situacao);
		builder.append(", dataAdmissao=");
		builder.append(dataAdmissao);
		builder.append(", dataFimContrato=");
		builder.append(dataFimContrato);
		builder.append(", dataInicioProf=");
		builder.append(dataInicioProf);
		builder.append(", universidade=");
		builder.append(universidade);
		builder.append(", niss=");
		builder.append(niss);
		builder.append(", dataValidadeDoc=");
		builder.append(dataValidadeDoc);
		builder.append(", numeroDoc=");
		builder.append(numeroDoc);
		builder.append(", tipoDoc=");
		builder.append(tipoDoc);
		builder.append(", descTipoDoc=");
		builder.append(descTipoDoc);
		builder.append(", arquivoDoc=");
		builder.append(arquivoDoc);
		builder.append(", codigoUnidadeOrganica=");
		builder.append(codigoUnidadeOrganica);
		builder.append(", areaCursoId=");
		builder.append(areaCursoId);
		builder.append("]");
		return builder.toString();
	}

}
