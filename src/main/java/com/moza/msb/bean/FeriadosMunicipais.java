/**
 *
 */
package com.moza.msb.bean;

import java.util.ArrayList;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class FeriadosMunicipais {

	ArrayList<FeriadoMunicipal> feriados;

	public FeriadosMunicipais() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FeriadosMunicipais(ArrayList<FeriadoMunicipal> feriados) {
		super();
		this.feriados = feriados;
	}

	public ArrayList<FeriadoMunicipal> getFeriados() {
		return this.feriados;
	}

	public void setFeriados(ArrayList<FeriadoMunicipal> feriados) {
		this.feriados = feriados;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("FeriadosMunicipais [feriados=");
		builder.append(feriados);
		builder.append("]");
		return builder.toString();
	}


}
