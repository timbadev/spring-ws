/**
 *
 */
package com.moza.msb.bean;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class FeriadoMunicipal {

	private String municipio;
	private String provincia;
	private String data;

	public String getMunicipio() {
		return this.municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getProvincia() {
		return this.provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getData() {
		return this.data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("FeriadoMunicipal [municipio=");
		builder.append(municipio);
		builder.append(", provincia=");
		builder.append(provincia);
		builder.append(", data=");
		builder.append(data);
		builder.append("]");
		return builder.toString();
	}



}
