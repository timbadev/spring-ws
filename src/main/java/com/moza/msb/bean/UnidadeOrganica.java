/**
 *
 */
package com.moza.msb.bean;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class UnidadeOrganica {

	private String codigo;
	private String nome;
	private String regiao;
	private String morada;
	private String codigoPostal;
	private String cidade;
	private String telefone;
	private String fax;
	private String email;
	private String unidadeSuperior;
	private String responsavelUnidade;

	public String getUnidadeSuperior() {
		return unidadeSuperior;
	}

	public String getResponsavelUnidade() {
		return responsavelUnidade;
	}

	public void setUnidadeSuperior(String unidadeSuperior) {
		this.unidadeSuperior = unidadeSuperior;
	}

	public void setResponsavelUnidade(String responsavelUnidade) {
		this.responsavelUnidade = responsavelUnidade;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRegiao() {
		return this.regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getMorada() {
		return this.morada;
	}

	public void setMorada(String morada) {
		this.morada = morada;
	}

	public String getCodigoPostal() {
		return this.codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("UnidadeOrganica [codigo=");
		builder.append(codigo);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", regiao=");
		builder.append(regiao);
		builder.append(", morada=");
		builder.append(morada);
		builder.append(", codigoPostal=");
		builder.append(codigoPostal);
		builder.append(", cidade=");
		builder.append(cidade);
		builder.append(", telefone=");
		builder.append(telefone);
		builder.append(", fax=");
		builder.append(fax);
		builder.append(", email=");
		builder.append(email);
		builder.append(", unidadeSuperior=");
		builder.append(unidadeSuperior);
		builder.append(", responsavelUnidade=");
		builder.append(responsavelUnidade);
		builder.append("]");
		return builder.toString();
	}
}
