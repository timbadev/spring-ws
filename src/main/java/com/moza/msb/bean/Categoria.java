package com.moza.msb.bean;

public class Categoria {
	private String CodigoCategoria;
	private String nome;

	public String getCodigoCategoria() {
		return CodigoCategoria;
	}

	public void setCodigoCategoria(String codigoCategoria) {
		CodigoCategoria = codigoCategoria;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
