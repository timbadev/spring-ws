package com.moza.msb.bean;

import java.util.ArrayList;

public class PeriodosFerias {

	private ArrayList<Periodo> periodos;

	public PeriodosFerias() {
	}

	public PeriodosFerias(ArrayList<Periodo> periodos) {
		this.periodos = periodos;
	}

	public ArrayList<Periodo> getPeriodos() {
		return periodos;
	}

	public void setPeriodos(ArrayList<Periodo> periodos) {
		this.periodos = periodos;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PeriodosFerias [periodos=");
		builder.append(periodos);
		builder.append("]");
		return builder.toString();
	}

}
