package com.moza.msb.bean;

import java.util.List;



/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class Colaboradores {
	
	public List<Colaborador> colaboradores;

	public List<Colaborador> getColaborador() {
		return colaboradores;
	}

	public void setColaborador(List<Colaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}

	public Colaboradores() {
		super();
	}

	public Colaboradores(List<Colaborador> colaboradores) {
		super();
		this.colaboradores = colaboradores;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Colaboradores [colaborador=");
		builder.append(colaboradores);
		builder.append("]");
		return builder.toString();
	}


}
