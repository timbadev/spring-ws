/**
 *
 */
package com.moza.msb.bean;

import java.util.ArrayList;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class Nacionalidades {
	private ArrayList<Nacionalidade> nacionalidade;

	public Nacionalidades(ArrayList<Nacionalidade> nacionalidade) {
		super();
		this.nacionalidade = nacionalidade;
	}

	public Nacionalidades() {
		super();

	}

	public ArrayList<Nacionalidade> getNacionalidade() {
		return this.nacionalidade;
	}

	public void setNacionalidade(ArrayList<Nacionalidade> nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Nacionalidades [nacionalidade=");
		builder.append(nacionalidade);
		builder.append("]");
		return builder.toString();
	}


}
