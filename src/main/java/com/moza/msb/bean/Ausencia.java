package com.moza.msb.bean;

public class Ausencia {
	private String numeroEmpregado;

	private String nomeAusencia;

	private String data;

	private String horaInicio;

	private String horaFim;

	private String duracao;

	public String getNumeroEmpregado() {
		return numeroEmpregado;
	}

	public void setNumeroEmpregado(String numeroEmpregado) {
		this.numeroEmpregado = numeroEmpregado;
	}

	public String getNomeAusencia() {
		return nomeAusencia;
	}

	public void setNomeAusencia(String nomeAusencia) {
		this.nomeAusencia = nomeAusencia;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	public String getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}

	public String getDuracao() {
		return duracao;
	}

	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Ausencia [numeroEmpregado=");
		builder.append(numeroEmpregado);
		builder.append(", nomeAusencia=");
		builder.append(nomeAusencia);
		builder.append(", data=");
		builder.append(data);
		builder.append(", horaInicio=");
		builder.append(horaInicio);
		builder.append(", horaFim=");
		builder.append(horaFim);
		builder.append(", duracao=");
		builder.append(duracao);
		builder.append("]");
		return builder.toString();
	}

}
