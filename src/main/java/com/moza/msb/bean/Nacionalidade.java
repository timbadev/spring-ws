/**
 *
 */
package com.moza.msb.bean;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class Nacionalidade {

	private String idNacionalidade;
	private String nome;
	private String descricao;

	public String getIdNacionalidade() {
		return this.idNacionalidade;
	}
	public void setIdNacionalidade(String idNacionalidade) {
		this.idNacionalidade = idNacionalidade;
	}
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return this.descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Nacionalidade [idNacionalidade=");
		builder.append(idNacionalidade);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", descricao=");
		builder.append(descricao);
		builder.append("]");
		return builder.toString();
	}

}
