/**
 *
 */
package com.moza.msb.bean;

import java.util.ArrayList;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class Habilitacoes {

	private ArrayList<Habilitacao> habilitacoes;

	public Habilitacoes() {
		super();
	}
	public Habilitacoes(ArrayList<Habilitacao> habilitacoes) {
		this.habilitacoes = habilitacoes;
	}

	public ArrayList<Habilitacao> getHabilitacoes() {
		return this.habilitacoes;
	}

	public void setHabilitacoes(ArrayList<Habilitacao> habilitacoes) {
		this.habilitacoes = habilitacoes;
	}
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Habilitacoes [habilitacoes=");
		builder.append(habilitacoes);
		builder.append("]");
		return builder.toString();
	}

}
