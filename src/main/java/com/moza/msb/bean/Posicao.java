package com.moza.msb.bean;

public class Posicao {
	private String codigoPosicao;
	private String nome;

	public String getCodigoPosicao() {
		return codigoPosicao;
	}

	public void setCodigoPosicao(String codigoPosicao) {
		this.codigoPosicao = codigoPosicao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
