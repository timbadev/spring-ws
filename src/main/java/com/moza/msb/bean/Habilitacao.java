/**
 *
 */
package com.moza.msb.bean;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class Habilitacao {

	private String idHabilitacao;
	private String nome;
	private String descricao;


	public String getIdHabilitacao() {
		return this.idHabilitacao;
	}
	public void setIdHabilitacao(String idHabilitacao) {
		this.idHabilitacao = idHabilitacao;
	}
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return this.descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Habilitacao [idHabilitacao=");
		builder.append(idHabilitacao);
		builder.append(", nome=");
		builder.append(nome);
		builder.append(", descricao=");
		builder.append(descricao);
		builder.append("]");
		return builder.toString();
	}


}
