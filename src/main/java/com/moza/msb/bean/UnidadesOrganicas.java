/**
 *
 */
package com.moza.msb.bean;

import java.util.ArrayList;

/**
 * @author fabio.fortes@mozabanco.co.mz
 *
 */
public class UnidadesOrganicas {
	ArrayList<UnidadeOrganica> unidadeOrganica;

	public UnidadesOrganicas() {
		super();
	}
	public UnidadesOrganicas(ArrayList<UnidadeOrganica> unidadesOrganicas) {
		this.unidadeOrganica = unidadesOrganicas;
	}

	public ArrayList<UnidadeOrganica> getUnidadesOrganicas() {
		return this.unidadeOrganica;
	}

	public void setUnidadesOrganicas(
			ArrayList<UnidadeOrganica> unidadesOrganicas) {
		this.unidadeOrganica = unidadesOrganicas;
	}
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("UnidadesOrganicas [unidadeOrganica=");
		builder.append(unidadeOrganica);
		builder.append("]");
		return builder.toString();
	}

}
